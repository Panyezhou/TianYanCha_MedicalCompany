# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html
from time import sleep
from scrapy import signals
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware
from fake_useragent import UserAgent

# 随机UserAgent
ua = UserAgent()


class UserAgentmiddleware(UserAgentMiddleware):
    """添加请求头"""

    def process_request(self, request, spider):

        # print('随机中间件...')
        f = open(r'cookie.txt')
        cookie_data = eval(f.read())

        cookie_str = ''                                         # cookie_str: selenium cookie格式 转 正常cookie格式
        for i in cookie_data:
            cookie_str += i['name'] + '=' + i['value'] + '; '

        request.headers["User-Agent"] = ua.random               # 随机UserAgent
        request.headers["Cookie:"] = cookie_str                 # 给请求添加cookie


class UserAgentmiddleware():
    """添加请求头"""
    def process_request(self, request, spider):
        if 'rnd=' or 'login' in request.url:                                    # 如果跳转到验证码页面和登陆页面
            if 'redirect_urls' in request.meta:                                 # meta的redirect_urls键里面会有当前url的跳转信息
                request._set_url(request.meta['redirect_urls'][0])              # 取跳转信息的第0个
                f = open('cookie.txt', mode='w')                                # 保存新的cookie
                f.write('wait...')                                              # 写入字符
        f = open(r'cookie.txt')
        if f.read() == 'wait...':                                               # 如果信息读出来时等待
            while True:                                                         # 循环判断Selenium是否拿到新的cookie
                sleep(0.5)
                print('wait cookie...')
                f = open(r'cookie.txt')
                if f.read() != 'wait...':
                    break
        sleep(0.2)
        f = open(r'cookie.txt')
        try:                                                                    # 拿到的cookie字符串转字典
            cookie_data = eval(f.read())
        except SyntaxError:                                                     # 在多个程序一起读写同一个文件时可能会报错
            sleep(0.8)
            f = open(r'cookie.txt')
            cookie_data = eval(f.read())
        f.close()
        cookie_str = ''                                                         # selenium cookie格式 转 正常cookie格式
        for i in cookie_data:
            cookie_str += i['name'] + '=' + i['value'] + '; '

        request.headers["User-Agent"] = ua.random                               # 随机UserAgent
        request.headers["Cookie:"] = cookie_str                                 # 给请求添加cookie


class TianyanchaMedicalcompanySpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class TianyanchaMedicalcompanyDownloaderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)
